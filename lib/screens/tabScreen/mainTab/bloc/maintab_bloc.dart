import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'maintab_event.dart';
part 'maintab_state.dart';

class MaintabBloc extends Bloc<MaintabEvent, MaintabState> {
  MaintabBloc() : super(MaintabInitial());

  @override
  Stream<MaintabState> mapEventToState(
    MaintabEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
