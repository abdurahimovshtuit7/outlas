import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:outlus/screens/tabScreen/bloc/bottom_navigation_bloc.dart';
import 'package:outlus/screens/tabScreen/view/badge.dart';

class CustomAppBar extends StatefulWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;
  final String title;
  final int pageIndex;

  CustomAppBar(this.title, this.pageIndex)
      : preferredSize = Size.fromHeight(50.0);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  String selectedText = 'Для мужчин'.tr();
  Widget ActionSheet(
    BuildContext context,
    String text,
  ) {
    return CupertinoActionSheetAction(
      child: Text(
        text,
        style: TextStyle(fontFamily: AppFont.SFRegular, fontSize: 14),
      ),
      onPressed: () {
        Navigator.pop(context, 'One');
        setState(() {
          selectedText = text;
        });
      },
    );
  }

  void actionSheet(BuildContext context) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        actions: <Widget>[
          ActionSheet(context, 'Для мужчин'.tr()),
          ActionSheet(context, 'Для женщин'.tr()),
          ActionSheet(context, 'Для детей'.tr()),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text(
            "очистить".tr(),
            style: TextStyle(
                fontFamily: AppFont.SFRegular,
                color: AppColor.pink,
                fontSize: 18),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      context.fallbackLocale;
    });
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
      builder: (BuildContext context, BottomNavigationState state) {
        return Container(
          height: 80,
          decoration: const BoxDecoration(
              border: Border(
            bottom: BorderSide(
              color: AppColor.warmGrey,
              width: 0.5,
            ),
          )),
          child: Padding(
            padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                      child: Row(
                        children: [
                          Text(
                            selectedText,
                            style: const TextStyle(color: AppColor.black),
                          ),
                          const SizedBox(width: 4),
                          Center(child: SvgPicture.asset(Images.downArrow))
                        ],
                      ),
                      onPressed: () => actionSheet(context)),
                  widget.pageIndex == 0
                      ? SvgPicture.asset(
                          Images.brandName,
                          color: AppColor.black,
                          width: 80,
                        )
                      : Text(
                          widget.title.toUpperCase(),
                          style: TextStyle(
                            fontFamily: AppFont.SFSemibold,
                            fontWeight: FontWeight.w600,
                            fontSize: 17,
                          ),
                        ),
                  Row(
                    children: [
                      SvgPicture.asset(Images.searchIcon),
                      SizedBox(width: 20),
                      Badge(
                          SvgPicture.asset(
                            Images.basket,
                          ),
                          2),
                      SizedBox(width: 10)
                    ],
                  ),
                ]),
          ),
        );
      },
    );
  }
}
