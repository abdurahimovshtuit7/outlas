part of 'bottom_navigation_bloc.dart';

abstract class BottomNavigationState extends Equatable {
  const BottomNavigationState();

  @override
  List<Object> get props => [];
}

class CurrentIndexChanged extends BottomNavigationState {
  final int currentIndex;
  final mainCatalogData = {};

  CurrentIndexChanged({required this.currentIndex});

  @override
  String toString() => 'CurrentIndexChanged to $currentIndex';
}

class PageLoading extends BottomNavigationState {
  @override
  String toString() => 'PageLoading';
}

class MainTabLoaded extends BottomNavigationState {
  final dynamic data;

  const MainTabLoaded({required this.data});

  // @override
  // String toString() => 'FirstPageLoaded with text: $text';
}

class FavoriteTabLoaded extends BottomNavigationState {
  final String number;

  FavoriteTabLoaded({required this.number});

  @override
  String toString() => 'SecondPageLoaded with number: $number';
}

class CategoriesTabLoaded extends BottomNavigationState {
  final String number;

  CategoriesTabLoaded({required this.number});

  @override
  String toString() => 'SecondPageLoaded with number: $number';
}

class PromotionTabLoaded extends BottomNavigationState {
  final String number;

  PromotionTabLoaded({required this.number});

  @override
  String toString() => 'SecondPageLoaded with number: $number';
}

class MoreTabLoaded extends BottomNavigationState {
  final String number;

  MoreTabLoaded({required this.number});

  @override
  String toString() => 'SecondPageLoaded with number: $number';
}
