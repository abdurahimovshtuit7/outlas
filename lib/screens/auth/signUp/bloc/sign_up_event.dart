abstract class SignUpEvent {}

class SignUpUsernameChanged extends SignUpEvent {
  final String username;

  SignUpUsernameChanged({required this.username});
}
class SignUpLastnameChanged extends SignUpEvent {
  final String lastname;

  SignUpLastnameChanged({required this.lastname});
}
class SignUpPhoneNumberChanged extends SignUpEvent {
  final String phonenumber;

  SignUpPhoneNumberChanged({required this.phonenumber});
}
class SignUpPasswordChanged extends SignUpEvent {
  final String password;

  SignUpPasswordChanged({required this.password});
}
class SignUpConfirmPasswordChanged extends SignUpEvent {
  final String confirmpassword;

  SignUpConfirmPasswordChanged({required this.confirmpassword});
}

class SignUpGenderChanged extends SignUpEvent {
  final bool gender;
  SignUpGenderChanged({required this.gender});
}


class SignUpSubmitted extends SignUpEvent {}
