import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';

class ParentItem extends StatelessWidget {
  final int index;
  ParentItem(this.index);
  // const ParentItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        height: 98,
        margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 10),
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            width: 1,
            color:
                index == 0 ? AppColor.pink : AppColor.warmGrey.withOpacity(0.4),
            style: BorderStyle.solid,
          ),
        ),
        child: SvgPicture.asset(Images.tShirt),
      ),
    );
  }
}
