import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:outlus/constants/images.dart';

class ButtonWithIcon extends StatelessWidget {
  final String title;
  final String iconName;
  final Function onPress;
  const ButtonWithIcon(this.title, this.iconName,this.onPress);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){onPress();},
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 24),
        child: Row(
          children: [
            SvgPicture.asset(iconName),
            SizedBox(
              width: 24,
            ),
            Expanded(child: Text(title)),
            SvgPicture.asset(Images.rightArrow)
          ],
        ),
      ),
    );
  }
}
