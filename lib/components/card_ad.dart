import 'package:flutter/material.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';

class CardAd extends StatelessWidget {
  final String productImage;
  final String productName;
  final String productDescription;
  final String price;
  final String oldPrice;
  final String discountPercent;
  CardAd(this.productName, this.productDescription, this.price,
      [this.productImage = Images.defaultImage,
      this.oldPrice = '',
      this.discountPercent = '']);

  //  CardAd(Key? key, this.productName, this.productDescription, this.price,
  //     [this.productImage = Images.defaultImage,
  //     this.oldPrice = '',
  //     this.discountPercent = '']);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        // margin: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            width: 1,
            color: AppColor.warmGrey.withOpacity(0.4),
            style: BorderStyle.solid,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              height: 224,
              alignment: Alignment.topRight,
              decoration: BoxDecoration(
                color: AppColor.warmGrey,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(4.0),
                  topRight: Radius.circular(4.0),
                ),
                image: DecorationImage(
                    image: AssetImage(
                      productImage.isNotEmpty
                          ? productImage
                          : Images.defaultImage,
                    ),
                    fit: BoxFit.cover),
              ),
              child: ElevatedButton(
                // ignore: prefer_const_constructors
                child: Icon(
                  Icons.favorite_border,
                  size: 15,
                  color: AppColor.pink,
                ),

                style: ElevatedButton.styleFrom(
                  primary: AppColor.white,
                  shape: const CircleBorder(),
                ),

                onPressed: () {},
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 14, left: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    productName,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: AppFont.SFBold,
                        fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 14),
                    child: Text(
                      productDescription,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontFamily: AppFont.SFRegular,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              price,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontFamily: AppFont.SFBold,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                            const SizedBox(height: 10),
                            oldPrice.isNotEmpty
                                ? Text(
                                    oldPrice,
                                    style: TextStyle(
                                        decoration: TextDecoration.lineThrough,
                                        fontFamily: AppFont.SFRegular,
                                        fontSize: 12,
                                        color: AppColor.gray),
                                  )
                                : Container()
                          ],
                        ),
                      ),
                      discountPercent.isNotEmpty
                          ? Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 6, vertical: 4),
                              color: AppColor.pinkOpacity,
                              child: Text(
                                '- $discountPercent %',
                                style: TextStyle(
                                    fontFamily: AppFont.SFBold,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: AppColor.pink),
                              ),
                            )
                          : Container()
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
