import 'package:flutter/material.dart';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:outlus/components/app_bar.dart';
import 'package:outlus/components/button.dart';
import 'package:outlus/components/input.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:easy_localization/easy_localization.dart';

class ForgotPassword extends StatefulWidget {
  static const routeName = 'forgot-password';
 
 ForgotPassword({Key? key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header('Забыли пароль'.tr(), true),
      body: SafeArea(
        bottom: false,
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                    child: Text(
                      'Мы отправим вам письмо со ссылкой для создания нового пароля',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 12, fontFamily: AppFont.SFRegular),
                    ),
                  ),
                  //Component for TextInput
                  // InputField('Номер телефона или адрес Эл. почты'.tr(), false),

                  Button(
                    "Отправить".tr(),
                    () {},
                    AppColor.pink,
                    AppColor.white,
                    0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
