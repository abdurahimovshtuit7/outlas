// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const Скидка = 'Скидка';
  static const Промокод = 'Промокод';
  static const Не указана = 'Не указана';
  static const Ошибка сервера = 'Ошибка сервера';
  static const Вернутся на главную = 'Вернутся на главную';
  static const Страница не найдено = 'Страница не найдено';
  static const нет в наличии = 'нет в наличии';
  static const Без размер = 'Без размер';
  static const Log in with Google = 'Log in with Google';
  static const Log in with Facebook = 'Log in with Facebook';
  static const Log in with Apple = 'Log in with Apple';
  static const Нет истории поиска = 'Нет истории поиска';
  static const Профиль = 'Профиль';
  static const Скидки = 'Скидки';
  static const Введите данные карты = 'Введите данные карты';
  static const С помощью = 'С помощью';
  static const Сумма = 'Сумма';
  static const Пополнить = 'Пополнить';
  static const Оплатить = 'Оплатить';
  static const Пополнить баланс = 'Пополнить баланс';
  static const Баланс = 'Баланс';
  static const OUTLAS Кошелек = 'OUTLAS Кошелек';
  static const Изменить язык = 'Изменить язык';
  static const {{count}} отзывов = '{{count}} отзывов';
  static const в наличии {{count}} шт = 'в наличии {{count}} шт';
  static const Открыть = 'Открыть';
  static const Удалить = 'Удалить';
  static const Edit Photo = 'Edit Photo';
  static const The app does not have access to the camera You can change this in your settings = 'The app does not have access to the camera You can change this in your settings';
  static const Сфотографировать = 'Сфотографировать';
  static const Unable to access images Please allow access if you want to be able to select images = 'Unable to access images Please allow access if you want to be able to select images';
  static const Выбрать из галереи = 'Выбрать из галереи';
  static const Код подтверждения = 'Код подтверждения';
  static const Введите код = 'Введите код';
  static const Не получили еще письмо с кодом = 'Не получили еще письмо с кодом';
  static const отправить заново = 'отправить заново';
  static const Подтвердить = 'Подтвердить';
  static const required = 'required';
  static const Забыли пароль = 'Забыли пароль';
  static const Мы отправим вам письмо со ссылкой для создания нового пароля = 'Мы отправим вам письмо со ссылкой для создания нового пароля';
  static const Номер телефона или адрес Эл почты = 'Номер телефона или адрес Эл почты';
  static const Отправить = 'Отправить';
  static const Придумайте новый пароль = 'Придумайте новый пароль';
  static const Новый пароль = 'Новый пароль';
  static const Повторите пароль = 'Повторите пароль';
  static const Изменить = 'Изменить';
  static const Passwords don't match = 'Passwords don't match';
  static const Войти в систему = 'Войти в систему';
  static const Чтобы войти в систему введите свой номер телефона или адрес электронной почты и пароль = 'Чтобы войти в систему введите свой номер телефона или адрес электронной почты и пароль';
  static const Пароль = 'Пароль';
  static const Войти = 'Войти';
  static const У вас нет аккаунта = 'У вас нет аккаунта';
  static const Создать аккаунт = 'Создать аккаунт';
  static const Имя = 'Имя';
  static const Фамилия = 'Фамилия';
  static const Мужской = 'Мужской';
  static const Женский = 'Женский';
  static const Я прочел (прочла) юридические условия = 'Я прочел (прочла) юридические условия';
  static const Политика конфиденциальности = 'Политика конфиденциальности';
  static const У вас уже есть аккаунт = 'У вас уже есть аккаунт';
  static const Корзина = 'Корзина';
  static const ИТОГО = 'ИТОГО';
  static const Продолжить = 'Продолжить';
  static const Изменить Email = 'Изменить Email';
  static const Ваш Email = 'Ваш Email';
  static const Вам будет отправлено письмо с кодом = 'Вам будет отправлено письмо с кодом';
  static const email must be a valid email = 'email must be a valid email';
  static const Изменить пароль = 'Изменить пароль';
  static const Текущий Пароль = 'Текущий Пароль';
  static const Пароль успешно изменен = 'Пароль успешно изменен';
  static const Изменить номер телефона = 'Изменить номер телефона';
  static const Номер телефона = 'Номер телефона';
  static const Оформление заказа = 'Оформление заказа';
  static const Товаров = 'Товаров';
  static const Редактировать = 'Редактировать';
  static const Способ оплаты = 'Способ оплаты';
  static const Выберите способ оплаты заказа = 'Выберите способ оплаты заказа';
  static const Оплата с помощью карты = 'Оплата с помощью карты';
  static const У вас пока нету карты! = 'У вас пока нету карты!';
  static const Добавить карту = 'Добавить карту';
  static const Я подтверждаю, что ознакомлен со следующими документами = 'Я подтверждаю, что ознакомлен со следующими документами';
  static const Правила и условия проведения акций = 'Правила и условия проведения акций';
  static const Нажмите здесь, чтобы узнать больше о Защите Покупателя = 'Нажмите здесь, чтобы узнать больше о Защите Покупателя';
  static const Доставка = 'Доставка';
  static const Ваш заказ принят успешно! = 'Ваш заказ принят успешно!';
  static const Выберите ваш адрес доставки = 'Выберите ваш адрес доставки';
  static const Выберите один из доступных адресов для доставки, вы всегда можете добавить/удалить не нужные адреса = 'Выберите один из доступных адресов для доставки, вы всегда можете добавить/удалить не нужные адреса';
  static const Выбрать тип доставки = 'Выбрать тип доставки';
  static const Пусто = 'Пусто';
  static const Адрес доставки = 'Адрес доставки';
  static const Добавить новый адрес = 'Добавить новый адрес';
  static const Мои отзывы = 'Мои отзывы';
  static const Контакты = 'Контакты';
  static const Эл почта = 'Эл почта';
  static const Адрес = 'Адрес';
  static const Социальные сети = 'Социальные сети';
  static const Название адреса = 'Название адреса';
  static const Указать на карте = 'Указать на карте';
  static const Имя получателя = 'Имя получателя';
  static const Почтовый индекс = 'Почтовый индекс';
  static const Добавить = 'Добавить';
  static const Название карты = 'Название карты';
  static const Номер карты = 'Номер карты';
  static const Срок действия = 'Срок действия';
  static const ММ/ГГ = 'ММ/ГГ';
  static const РЕДАКТИРОВАТЬ КАРТУ = 'РЕДАКТИРОВАТЬ КАРТУ';
  static const Личные данные = 'Личные данные';
  static const Невозможно получить доступ к изображениям Пожалуйста, разрешите доступ, если вы хотите иметь возможность выбирать изображения = 'Невозможно получить доступ к изображениям Пожалуйста, разрешите доступ, если вы хотите иметь возможность выбирать изображения';
  static const Галерея = 'Галерея';
  static const Приложение не имеет доступа к камере Вы можете изменить это в своих настройках = 'Приложение не имеет доступа к камере Вы можете изменить это в своих настройках';
  static const Камера = 'Камера';
  static const Отчество = 'Отчество';
  static const Дата рождения = 'Дата рождения';
  static const Ваш пол = 'Ваш пол';
  static const Сохранить изменения = 'Сохранить изменения';
  static const Помощь = 'Помощь';
  static const Бренды = 'Бренды';
  static const очистить = 'очистить';
  static const Отменить = 'Отменить';
  static const Применить = 'Применить';
  static const Категория = 'Категория';
  static const Цвет = 'Цвет';
  static const Фильтр = 'Фильтр';
  static const Все = 'Все';
  static const Размер = 'Размер';
  static const Размеры = 'Размеры';
  static const Разрешение на местоположение не было предоставлено Пожалуйста, разрешите доступ = 'Разрешение на местоположение не было предоставлено Пожалуйста, разрешите доступ';
  static const Локация скопировано в буфер обмена = 'Локация скопировано в буфер обмена';
  static const Загрузка = 'Загрузка';
  static const МОЙ АККАУНТ = 'МОЙ АККАУНТ';
  static const Вы уверены, что хотите удалить = 'Вы уверены, что хотите удалить';
  static const У вас пока нету адреса для доставки = 'У вас пока нету адреса для доставки';
  static const Мои карты = 'Мои карты';
  static const Управление вашими картами = 'Управление вашими картами';
  static const У вас пока нету карты = 'У вас пока нету карты';
  static const Для мужчин = 'Для мужчин';
  static const Для женщин = 'Для женщин';
  static const Для детей = 'Для детей';
  static const Посмотреть = 'Посмотреть';
  static const Закрыть = 'Закрыть';
  static const История заказов = 'История заказов';
  static const Заказ = 'Заказ';
  static const Цена = 'Цена';
  static const Информация о заказах = 'Информация о заказах';
  static const Отследить ваш заказ = 'Отследить ваш заказ';
  static const Выбрать размер = 'Выбрать размер';
  static const Гид по размерам = 'Гид по размерам';
  static const ВАМ ТАКЖЕ МОЖЕТ ПОНРАВИТЬСЯ = 'ВАМ ТАКЖЕ МОЖЕТ ПОНРАВИТЬСЯ';
  static const ОТЗЫВЫ = 'ОТЗЫВЫ';
  static const Показать все = 'Показать все';
  static const Количество = 'Количество';
  static const Успешно добавлено в корзину = 'Успешно добавлено в корзину';
  static const Ваш отзыв = 'Ваш отзыв';
  static const Поставьте рейтинг = 'Поставьте рейтинг';
  static const Продавец = 'Продавец';
  static const Цена (от высокой к низкой) = 'Цена (от высокой к низкой)';
  static const Цена (от низкой к высокой) = 'Цена (от низкой к высокой)';
  static const В алфавитном порядке (А-Я) = 'В алфавитном порядке (А-Я)';
  static const В алфавитном порядке (Я-А) = 'В алфавитном порядке (Я-А)';
  static const СОРТИРОВКА = 'СОРТИРОВКА';
  static const ФИЛЬТР = 'ФИЛЬТР';
  static const Сортировка = 'Сортировка';
  static const Продукты = 'Продукты';
  static const Категории = 'Категории';
  static const Поиск по бренду, продукту или категории = 'Поиск по бренду, продукту или категории';
  static const ЖЕНСКИЙ = 'ЖЕНСКИЙ';
  static const МУЖСКОЙ = 'МУЖСКОЙ';
  static const ДЕТСКИЙ = 'ДЕТСКИЙ';
  static const ВСЕ = 'ВСЕ';
  static const ИЗБРАННЫЕ = 'ИЗБРАННЫЕ';
  static const Дата = 'Дата';
  static const ТОП ПРОДАЖ = 'ТОП ПРОДАЖ';
  static const ГОРЯЧИЕ СКИДКИ = 'ГОРЯЧИЕ СКИДКИ';
  static const КАТАЛОГ = 'КАТАЛОГ';
  static const ПОПУЛЯРНЫЕ БРЕНДЫ = 'ПОПУЛЯРНЫЕ БРЕНДЫ';
  static const ПРЕМИУМ БРЕНДЫ = 'ПРЕМИУМ БРЕНДЫ';
  static const Войти / регистрация = 'Войти / регистрация';
  static const Выйти из системы = 'Выйти из системы';
  static const Logout = 'Logout';
  static const Вы уверены, что хотите выйти? = 'Вы уверены, что хотите выйти?';
  static const Домой = 'Домой';
  static const Избранные = 'Избранные';
  static const Обновите приложение до последней версии, чтобы продолжить = 'Обновите приложение до последней версии, чтобы продолжить';
  static const Обновить = 'Обновить';

}
