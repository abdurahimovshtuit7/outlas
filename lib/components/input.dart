import 'package:flutter/material.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';

class InputField extends StatefulWidget {
  final String label;
  final bool icon;
  final dynamic onChange;
  final dynamic validator;
  final dynamic keyboardType;

  InputField(this.label, this.icon, this.onChange, this.validator,{this.keyboardType = TextInputType.text});

  @override
  _InputFieldState createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  bool hidden = true;
  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Container(
        margin: EdgeInsets.symmetric(
          // horizontal: MediaQuery.of(context).size.width * 0.1,
          vertical: 13,
        ),
        child: TextFormField(
          obscureText: widget.icon ? hidden : !hidden,
          // autofocus: true,
          keyboardType: widget.keyboardType,
          decoration: InputDecoration(
            hintText: widget.label,
            hintStyle: TextStyle(fontSize: 14, fontFamily: AppFont.SFRegular),
            fillColor: AppColor.white,
            filled: true,
            suffixIcon: widget.icon
                ? IconButton(
                    onPressed: () => {
                      setState(() {
                        hidden = !hidden;
                      })
                    },
                    icon: hidden
                        ? Icon(Icons.visibility_off)
                        : Icon(Icons.visibility),
                  )
                : null,
            enabledBorder: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: AppColor.warmGrey.withOpacity(0.3)),
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: AppColor.pink),
            ),
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0)),
              borderSide: BorderSide(color: Colors.pink),
            ),
          ),
          style: TextStyle(fontSize: 14, fontFamily: AppFont.SFRegular),
          cursorColor: AppColor.primary,
          validator: (String? value) => widget.validator(value),
          onChanged: (value) => widget.onChange(value),
        ),
      ),
    ]);
  }
}
