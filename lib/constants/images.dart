class Images {
  Images._();

  //from icons directory
  static const String brandName = 'assets/svg/brand_name.svg';
  static const String downArrow = 'assets/svg/down_arrow.svg';
  static const String searchIcon = 'assets/svg/search_icon.svg';
  static const String basket = 'assets/svg/basket.svg';
  static const String homeIcon = 'assets/svg/home_icon.svg';
  static const String favouriteIcon = 'assets/svg/favourite_icon.svg';
  static const String menu = 'assets/svg/menu.svg';
  static const String rebateIcon = 'assets/svg/rebate_icon.svg';
  static const String accountIcon = 'assets/svg/account.svg';
  static const String adidas = 'assets/svg/adidas.svg';
  static const String tShirt = 'assets/svg/t_shirt.svg';
  static const String allIcon = 'assets/svg/all_icon.svg';
  static const String questionMark = 'assets/svg/question_mark.svg';
  static const String contactIcon = 'assets/svg/contact.svg';
  static const String rightArrow = 'assets/svg/right_arrow.svg';
  static const String facebookLogo = 'assets/svg/facebook_logo.svg';
  static const String googleLogo = 'assets/svg/google.svg';
  static const String leftArrow = 'assets/svg/left_arrow.svg';
  static const String crown = 'assets/svg/crown.svg';

  //from images directory
  static const String boyImage = 'assets/images/boy.jpg';
  static const String girlImage = 'assets/images/girl.jpg';
  static const String childImage = 'assets/images/child.jpg';
  static const String product = 'assets/images/product.jpg';
  static const String gucci = 'assets/images/gucci.jpg';
  static const String defaultImage = 'assets/images/defaultImage.jpg';
}
