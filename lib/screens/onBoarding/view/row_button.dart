import 'package:flutter/material.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:easy_localization/easy_localization.dart';


class RowButton extends StatelessWidget {
  final int currentPage;
  final int page;
  final String label;
  final Function onPress;

  RowButton(this.currentPage, this.page, this.label, this.onPress);
  // const RowButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
              color: currentPage == page ? Colors.white : Colors.transparent),
        ),
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(0),
          primary: currentPage == page
              ? Colors.white
              : Colors.white.withOpacity(0.5),
          textStyle: const TextStyle(fontSize: 14),
        ),
        onPressed: () => onPress(),
        child: Text(
          label.toUpperCase(),
          style: TextStyle(fontFamily: AppFont.SFRegular),
        ),
      ),
    );
  }
}
