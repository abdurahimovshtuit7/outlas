import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'categories_screen_event.dart';
part 'categories_screen_state.dart';

class CategoriesScreenBloc extends Bloc<CategoriesScreenEvent, CategoriesScreenState> {
  CategoriesScreenBloc() : super(CategoriesScreenInitial());

  @override
  Stream<CategoriesScreenState> mapEventToState(
    CategoriesScreenEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
