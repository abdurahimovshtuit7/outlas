import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outlus/repositories/auth_repository.dart';
import 'package:outlus/screens/auth/signUp/bloc/form_submission_status.dart';
import 'package:outlus/screens/auth/signUp/bloc/sign_up_event.dart';
import 'package:outlus/screens/auth/signUp/bloc/sign_up_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  final AuthRepository authRepo;

  SignUpBloc({required this.authRepo}) : super(SignUpState());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    // Username updated
    if (event is SignUpUsernameChanged) {
      yield state.copyWith(username: event.username);
    } else if (event is SignUpLastnameChanged) {
      yield state.copyWith(lastname: event.lastname);
    } else if (event is SignUpPhoneNumberChanged) {
      yield state.copyWith(phonenumber: event.phonenumber);
    } else if (event is SignUpPasswordChanged) {
      yield state.copyWith(password: event.password);
    } else if (event is SignUpConfirmPasswordChanged) {
      yield state.copyWith(confirmpassword: event.confirmpassword);
    } else if (event is SignUpGenderChanged) {
      yield state.copyWith(gender: event.gender);
    } else if (event is SignUpSubmitted) {
      yield state.copyWith(formStatus: FormSubmitting());

      try {
        await authRepo.signUp(
          state.username,
          state.lastname,
          state.phonenumber,
          state.password,
          state.confirmpassword,
          state.gender,
        );
        yield state.copyWith(formStatus: SubmissionSuccess());
      } catch (e) {
        yield state.copyWith(formStatus: SubmissionFailed(e));
      }
    }
  }
}
