import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';

class Button extends StatelessWidget {
  final String text;
  final Function onPressedAction;
  final Color backgroundColor;
  final Color textColor;
  final double border;
  final bool progress;

  Button(this.text, this.onPressedAction, this.backgroundColor, this.textColor,
      this.border,
      [this.progress = false]);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 20),
      child: progress
          ? TextButton(
              onPressed: null,
              child: Container(
                width: 15,
                height: 15,
                child: const CircularProgressIndicator(
                  // color: AppColor.white,
                  strokeWidth: 3,
                ),
              ),
              style: ElevatedButton.styleFrom(
                primary: backgroundColor.withOpacity(0.5),
                padding: const EdgeInsets.symmetric(vertical: 15),
                side: border != 0
                    ? BorderSide(width: border, color: textColor)
                    : null,
                textStyle: TextStyle(
                    fontSize: 14,
                    fontFamily: AppFont.SFRegular,
                    color: textColor),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                onSurface: backgroundColor,
              ),
            )
          : TextButton(
              onPressed: () => onPressedAction(),
              child: Text(
                text,
                style: TextStyle(color: textColor),
              ),
              style: ElevatedButton.styleFrom(
                primary: backgroundColor,
                padding: const EdgeInsets.symmetric(vertical: 15),
                side: border != 0
                    ? BorderSide(width: border, color: textColor)
                    : null,
                textStyle: TextStyle(
                    fontSize: 14,
                    fontFamily: AppFont.SFRegular,
                    color: textColor),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
            ),
    );
  }
}
