import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';

class BrandItem extends StatelessWidget {
  const BrandItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            width: 1,
            color: AppColor.warmGrey.withOpacity(0.4),
            style: BorderStyle.solid,
          ),
        ),
        child: SvgPicture.asset(Images.adidas),
      ),
    );
  }
}
