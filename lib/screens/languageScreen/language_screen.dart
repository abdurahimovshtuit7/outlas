import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outlus/components/app_bar.dart';
import 'package:outlus/routes.dart';
import 'package:outlus/screens/auth/signIn/sign_in.dart';
// import 'package:outlus/constants/colors.dart';
import 'package:outlus/screens/languageScreen/view/button.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:outlus/screens/tabScreen/bloc/bottom_navigation_bloc.dart';

class LanguageScreen extends StatefulWidget {
  static const String routeName = 'lang-screen';
  const LanguageScreen({Key? key}) : super(key: key);

  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  bool isSelected = false;

  final langs = const [
    {'title': "Русский", 'icon': "РУ", 'code': "ru"},
    {'title': "O’zbekcha", 'icon': "UZ", 'code': "uz"},
    {'title': "English", 'icon': "EN", 'code': "en"}
  ];

  void onChanged(
    BuildContext context,
    String langCode,
  ) {
    context.setLocale(Locale(langCode));
  }

  @override
  Widget build(BuildContext context) {
    print(context.fallbackLocale.toString());
    return Scaffold(
      appBar: Header('Изменить язык'.tr(), true),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
          
            children: [
              InkWell(
                onTap: (){
                  //  Navigator.of(context).push(
                  // MaterialPageRoute(builder: (context) => LoginPage()));
                   Navigator.of(context,rootNavigator: true).pushNamed(Routes.signIn);
                },
                child: Container(child: Text("data"),),
              ),
              ...langs.asMap().entries.map((entry) {
                int index = entry.key;
                dynamic value = entry.value;
                return LanguageButton(
                  value['title'],
                  value['icon'],
                  value['code'] == context.locale.toString(),
                  () {
                    onChanged(context, value['code']);
                  },
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
