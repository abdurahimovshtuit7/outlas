import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:one_context/one_context.dart';
import 'package:outlus/routes.dart';
import 'package:outlus/screens/onBoarding/bloc/onboarding_bloc.dart';
import 'package:outlus/screens/tabScreen/bloc/bottom_navigation_bloc.dart';
import 'package:outlus/screens/tabScreen/tab_screen.dart';
import './translations/codegen_loader.g.dart';

// import './pages/auth/login/login_page.dart';
import 'constants/app_theme.dart';
import 'repositories/repositories.dart';

// import 'package:easy_localization/easy_localization.dart';
class SimpleBlocObserver extends BlocObserver {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await EasyLocalization.ensureInitialized();
  // bool platform = UniversalPlatform.isIOS;
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, // transparent status bar
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.dark // dark text for status bar
      ));
  Bloc.observer = SimpleBlocObserver();

  runApp(
    EasyLocalization(
        supportedLocales: const [
          Locale('en'),
          Locale('ru'),
          Locale('uz'),
        ],
        path:
            'assets/translations', // <-- change the path of the translation files
        fallbackLocale: const Locale('ru'),
        startLocale: const Locale('ru'),
        assetLoader: const CodegenLoader(),
        useOnlyLangCode: true,
        child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final GlobalKey<NavigatorState> navigatorKey =
       GlobalKey<NavigatorState>();
  @override
  Widget build(BuildContext context) {
    // FlutterStatusbarcolor.setStatusBarColor(Colors.white);
    // ignore: avoid_print
    print(context.locale); // output: en_US

    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
      // title: 'Flutter Demo',
      theme: themeData,
      initialRoute: Routes.onBoarding,
      // home: Routes.onBoarding,
      routes: Routes.routes,
      // navigatorKey: OneContext().navigator.key

    );
    // routes: Routes.routes,
  }
}
