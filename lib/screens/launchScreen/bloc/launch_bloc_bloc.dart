import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'launch_bloc_event.dart';
part 'launch_bloc_state.dart';

class LaunchBlocBloc extends Bloc<LaunchBlocEvent, LaunchBlocState> {
  LaunchBlocBloc() : super(LaunchBlocInitial());

  @override
  Stream<LaunchBlocState> mapEventToState(
    LaunchBlocEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
