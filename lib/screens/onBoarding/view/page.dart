import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:outlus/constants/images.dart';

class OnboardingPage extends StatelessWidget {
  final String image;
  OnboardingPage(this.image);
  // const OnboardingPage({imae? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                image,
              ),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(),
        ),
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xff000000).withOpacity(0),
                Color(0xff000000).withOpacity(0.4),
                Color(0xff000000).withOpacity(0.9),
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: SvgPicture.asset(
            Images.brandName,
          ),
        ),
      ],
    );
  }
}
