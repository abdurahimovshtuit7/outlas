import 'package:flutter/material.dart';
import 'package:outlus/constants/colors.dart';

class LanguageButton extends StatelessWidget {
  final String langTitle;
  final String langCode;
  final bool isSelected;
  final Function onPress;

  LanguageButton(this.langTitle, this.langCode, this.isSelected, this.onPress);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        await onPress();
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        margin: EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            width: 1,
            color: AppColor.warmGrey.withOpacity(0.2),
            style: BorderStyle.solid,
          ),
        ),
        child: Row(
          children: [
            Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  // color: AppColor.white,
                  borderRadius: BorderRadius.circular(120),
                  border: Border.all(
                    width: 1,
                    color: AppColor.warmGrey.withOpacity(0.2),
                    style: BorderStyle.solid,
                  ),
                ),
                child: Text(langCode)),
            SizedBox(
              width: 15,
            ),
            Expanded(child: Text(langTitle)),
            Icon(
              isSelected ? Icons.radio_button_on : Icons.radio_button_off,
              size: 26,
              color: AppColor.pink,
            ),
          ],
        ),
      ),
    );
  }
}
