import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';

class ButtonWithIcon extends StatelessWidget {
  final String icon;
  final String text;
  final Function onPressedAction;
  final Color borderColor;
  final Color textColor;

  ButtonWithIcon(this.icon, this.text, this.onPressedAction, this.borderColor,
      this.textColor);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 8),
      child: TextButton(
        onPressed: () => onPressedAction(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icon,
              height: 24,
              width: 24,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              text,
              style: TextStyle(color: textColor),
            ),
          ],
        ),
        style: ElevatedButton.styleFrom(
          primary: AppColor.white,
          padding: const EdgeInsets.symmetric(vertical: 15),
          side: BorderSide(width: 1, color: borderColor.withOpacity(0.5)),
          textStyle: TextStyle(
              fontSize: 14, fontFamily: AppFont.SFRegular, color: textColor),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
        ),
      ),
    );
  }
}
