import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:outlus/model/main_catalog_model.dart';

import '../../../repositories/repositories.dart';

part 'bottom_navigation_event.dart';
part 'bottom_navigation_state.dart';

class BottomNavigationBloc
    extends Bloc<BottomNavigationEvent, BottomNavigationState> {
  BottomNavigationBloc(
      {required this.mainTabRepository,
      required this.favoriteTabRepository,
      required this.categoriesTabRepository,
      required this.promotionTabRepository,
      required this.moreTabRepository})
      : assert(favoriteTabRepository != null),
        assert(categoriesTabRepository != null),
        assert(promotionTabRepository != null),
        assert(moreTabRepository != null),
        super(PageLoading());

  final MainTabRepository mainTabRepository;
  final FavoriteTabRepository favoriteTabRepository;
  final CategoriesTabRepository categoriesTabRepository;
  final PromotionTabRepository promotionTabRepository;
  final MoreTabRepository moreTabRepository;
  int currentIndex = 0;

  @override
  Stream<BottomNavigationState> mapEventToState(
      BottomNavigationEvent event) async* {
    if (event is AppStarted) {
      add(PageTapped(index: currentIndex));
    }
    if (event is PageTapped) {
      currentIndex = event.index;
      yield CurrentIndexChanged(currentIndex: currentIndex);
      yield PageLoading();

      if (currentIndex == 0) {
        dynamic data = await _getMainTabData();
        yield MainTabLoaded(data: data);
      }
      if (currentIndex == 1) {
        String data = await _getFavoriteTabData();
        yield FavoriteTabLoaded(number: data);
      }
      if (currentIndex == 2) {
        String data = await _getCategoriesTabData();
        yield CategoriesTabLoaded(number: data);
      }
      if (currentIndex == 3) {
        String data = await _getPromotionTabData();
        yield PromotionTabLoaded(number: data);
      }
      if (currentIndex == 4) {
        String data = await _getMoreTabData();
        yield MoreTabLoaded(number: data);
      }
    }
    if (event is ChangeLanguage) {
      if (currentIndex == 4) {
        String data = await _getMoreTabData();
        yield MoreTabLoaded(number: data);
      }
    }
  }

  Future<dynamic> _getMainTabData() async {
    dynamic data = mainTabRepository.data;
    // print('data1:${data.data[0].category.children[0].name}');

    if (data == '') {
      await mainTabRepository.getMainCatalog();
      data = mainTabRepository.data;
      // ignore: avoid_print
      print('data:${data.data[0].category.children[0].name}');
    }
    return data;
  }

  Future<String> _getFavoriteTabData() async {
    String data = favoriteTabRepository.data;
    if (data.isEmpty) {
      await favoriteTabRepository.fetchData();
      data = favoriteTabRepository.data;
    }
    return data;
  }

  Future<String> _getCategoriesTabData() async {
    String data = categoriesTabRepository.data;
    if (data.isEmpty) {
      await categoriesTabRepository.fetchData();
      data = categoriesTabRepository.data;
    }
    return data;
  }

  Future<String> _getPromotionTabData() async {
    String data = promotionTabRepository.data;
    if (data.isEmpty) {
      await promotionTabRepository.fetchData();
      data = promotionTabRepository.data;
    }
    return data;
  }

  Future<String> _getMoreTabData() async {
    String data = moreTabRepository.data;
    if (data.isEmpty) {
      await moreTabRepository.fetchData();
      data = moreTabRepository.data;
    }
    return data;
  }
}
