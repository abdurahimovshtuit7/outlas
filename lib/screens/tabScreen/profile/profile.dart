import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:outlus/components/app_bar.dart';
import 'package:outlus/components/button_with_icon.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';
import 'package:outlus/routes.dart';
import 'package:outlus/screens/languageScreen/language_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);
  static const routeName = 'profile-screen';

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    setState(() {
      context.fallbackLocale;
    });
    return SafeArea(
      child: Scaffold(
        appBar: Header('Профиль'.tr(), false),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed(Routes.signIn);
                },
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                  decoration: BoxDecoration(
                    // color: AppColor.white,
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                      width: 1,
                      color: AppColor.pink,
                      style: BorderStyle.solid,
                    ),
                  ),
                  child: Text(
                    'Войти / регистрация',
                    style: TextStyle(
                      fontFamily: AppFont.SFRegular,
                      fontSize: 15,
                      color: AppColor.pink,
                    ),
                  ).tr(),
                ),
              ),
            ),
            const Divider(thickness: 1),
            ButtonWithIcon('Помощь'.tr(), Images.questionMark, () {}),
            Divider(thickness: 1),
            ButtonWithIcon('Контакты'.tr(), Images.contactIcon, () {}),
            Divider(thickness: 1),
            ButtonWithIcon('Изменить язык'.tr(), Images.contactIcon, () {
          
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => LanguageScreen()));
              // Navigator.of(context,rootNavigator: true).pushNamed(Routes.languageScreen);
            }),
            Divider(thickness: 1),
          ],
        ),
      ),
    );
  }
}
