import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:outlus/components/app_bar.dart';
import 'package:outlus/components/button.dart';
import 'package:outlus/components/input.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/repositories/auth_repository.dart';
import 'package:outlus/screens/auth/codeEntry/bloc/code_entry_bloc.dart';
import 'package:outlus/screens/auth/codeEntry/code_entry.dart';
import 'package:outlus/screens/auth/signUp/bloc/sign_up_bloc.dart';

import '../../../routes.dart';
import 'bloc/form_submission_status.dart';
import 'bloc/sign_up_event.dart';
import 'bloc/sign_up_state.dart';

class SignUp extends StatefulWidget {
  static const routeName = 'sign-up';

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void onPress() {}

  Widget _checkButton(BuildContext context, bool active, String text) {
    var state = context.read<SignUpBloc>().state;
    return Material(
      child: InkWell(
        onTap: () {
          context.read<SignUpBloc>().add(SignUpGenderChanged(gender: active));
        },
        child: Row(
          children: [
            Icon(
              active
                  ? (state.gender
                      ? Icons.radio_button_on
                      : Icons.radio_button_off)
                  : (state.gender
                      ? Icons.radio_button_off
                      : Icons.radio_button_on),
              color: AppColor.pink,
              size: 26,
            ),
            const SizedBox(width: 8),
            Text(
              text,
              style: TextStyle(
                fontFamily: AppFont.SFMedium,
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ).tr(),
          ],
        ),
      ),
    );
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Route _createRoute(BuildContext context) {
    var state = context.read<SignUpBloc>().state;
    String name = state.username;
    String lastname = state.lastname;
    String username = state.phonenumber;
    String gender = state.gender ? 'm' : 'f';
    String password = state.password;
    String passwordConfirmation = state.confirmpassword;
    return PageRouteBuilder(
      pageBuilder: (BuildContext context, animation, secondaryAnimation) =>
          RepositoryProvider(
        create: (context) => AuthRepository(),
        child: BlocProvider<CodeEntryBloc>(
          create: (context) => CodeEntryBloc(authRepo: AuthRepository()),
          child: CodeEntry(
            name: name,
            lastname: lastname,
            username: username,
            gender: gender,
            password: password,
            passwordConfirmation: passwordConfirmation,
          ),
        ),
      ),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = const Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header('Создать аккаунт'.tr(), true),
      body: SafeArea(
          bottom: false,
          child: BlocProvider(
            create: (context) =>
                SignUpBloc(authRepo: context.read<AuthRepository>()),
            child: BlocListener<SignUpBloc, SignUpState>(
              listener: (BuildContext context, SignUpState state) {
                final formStatus = state.formStatus;
                if (formStatus is SubmissionFailed) {
                  _showSnackBar(context, formStatus.exception.toString());
                } else if (formStatus is SubmissionSuccess) {
                  Navigator.of(context)
                      .push(
                        _createRoute(context),
                      )
                      .then((result) {});
                  // Navigator.of(context).push(_createRoute()).then((result) {});
                }
              },
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: BlocBuilder<SignUpBloc, SignUpState>(
                          builder: (BuildContext context, SignUpState state) {
                        return Column(
                          children: [
                            InputField(
                              'Имя'.tr(),
                              false,
                              (value) => context.read<SignUpBloc>().add(
                                    SignUpUsernameChanged(username: value),
                                  ),
                              (value) => state.isValidUsername
                                  ? null
                                  : 'Username is too short',
                            ),
                            InputField(
                              'Фамилия'.tr(),
                              false,
                              (value) => context.read<SignUpBloc>().add(
                                    SignUpLastnameChanged(lastname: value),
                                  ),
                              (value) => state.isValidLastname
                                  ? null
                                  : 'Lastname is too short',
                            ),

                            //Component for TextInput
                            InputField(
                              'Номер телефона или адрес Эл. почты'.tr(),
                              false,
                              (value) => context.read<SignUpBloc>().add(
                                    SignUpPhoneNumberChanged(
                                        phonenumber: value),
                                  ),
                              (value) => state.isValidPhonenumber
                                  ? null
                                  : 'Phone number is too short',
                            ),
                            //Component for TextInput
                            InputField(
                              'Пароль'.tr(),
                              true,
                              (value) => context.read<SignUpBloc>().add(
                                    SignUpPasswordChanged(password: value),
                                  ),
                              (value) => state.isValidPassword
                                  ? null
                                  : 'Password is too short',
                            ),
                            InputField(
                              'Повторите пароль'.tr(),
                              true,
                              (value) => context.read<SignUpBloc>().add(
                                    SignUpConfirmPasswordChanged(
                                        confirmpassword: value),
                                  ),
                              (value) => state.isValidConfirmpassword
                                  ? null
                                  : 'Password not equal',
                            ),

                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: Row(
                                children: [
                                  _checkButton(context, true, 'Мужской'),
                                  const SizedBox(width: 30),
                                  _checkButton(context, false, 'Женский'),
                                ],
                              ),
                            ),
                            //Component for Button
                            Button("Создать аккаунт".tr(), () {
                              if (_formKey.currentState!.validate()) {
                                context
                                    .read<SignUpBloc>()
                                    .add(SignUpSubmitted());
                              }
                            }, AppColor.pink, AppColor.white, 0,
                                state.formStatus is FormSubmitting),
                            Center(
                                child: Text('У вас уже есть аккаунт?',
                                        style: TextStyle(
                                            fontFamily: AppFont.SFRegular))
                                    .tr()),
                            Button(
                              "Войти в систему".tr(),
                              onPress,
                              AppColor.white,
                              AppColor.pink,
                              1,
                            ),
                          ],
                        );
                      })),
                ),
              ),
            ),
          )),
    );
  }
}
