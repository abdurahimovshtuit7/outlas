class AppFont {
  AppFont._();

  static String SFBold = 'SFProDisplay-Bold';
  static String SFRegular = 'SFProDisplay-Regular';
  static String SFMedium = 'SFProDisplay-Medium';
  static String SFSemibold = 'SFProDisplay-Semibold';
  static String SFThin = 'SFProDisplay-Thin';
  static String SFBlack = 'SFProDisplay-Black';
}
