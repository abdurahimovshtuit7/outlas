import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outlus/repositories/auth_repository.dart';
import 'package:outlus/screens/auth/codeEntry/bloc/code_entry_state.dart';
import 'package:outlus/screens/auth/codeEntry/bloc/form_submission_status.dart';

import 'code_entry_event.dart';

class CodeEntryBloc extends Bloc<CodeEntryEvent, CodeEntryState> {
  final AuthRepository authRepo;

  CodeEntryBloc({required this.authRepo}) : super(CodeEntryState());

  @override
  Stream<CodeEntryState> mapEventToState(CodeEntryEvent event) async* {
    // Username updated
    if (event is CodeChanged) {
      yield state.copyWith(code: event.code);

      // Password updated
    } else if (event is WriteData) {
      yield state.copyWith(
          username: event.username,
          name: event.name,
          lastname: event.lastname,
          gender: event.gender,
          password: event.password,
          passwordConfirmation: event.passwordConfirmation);
    } else if (event is ApprovePhone) {
      yield state.copyWith(formStatus: FormSubmitting());

      try {
        // print('object:${state.username}');
        await authRepo.approvePhone(
            state.code,
            state.username,
            state.name,
            state.lastname,
            state.gender,
            state.password,
            state.passwordConfirmation);
        yield state.copyWith(formStatus: SubmissionSuccess());
      } catch (e) {
        yield state.copyWith(formStatus: SubmissionFailed(e));
      }
    }
  }
}
