// import 'package:meta/meta.dart';
import 'dart:convert';

class MainCatalogModel {
  MainCatalogModel({
    required this.success,
    required this.data,
    required this.links,
    required this.errors,
  });

  final bool success;
  final List<Datum> data;
  final Links links;
  final dynamic errors;

  factory MainCatalogModel.fromJson(Map<String, dynamic> json) =>
      MainCatalogModel(
        success: json["success"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        links: Links.fromJson(json["links"]),
        errors: json["errors"],
      );
}

class Datum {
  Datum({
    required this.id,
    required this.name,
    required this.mImageUrl,
    required this.brand,
    required this.category,
    required this.subcategory,
  });

  final int id;
  final String name;
  final String mImageUrl;
  final Brand brand;
  final Category category;
  final Brand subcategory;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        mImageUrl: json["m_image_url"],
        brand: Brand.fromJson(json["brand"]),
        category: Category.fromJson(json["category"]),
        subcategory: Brand.fromJson(json["subcategory"]),
      );
}

class Brand {
  Brand({
    required this.id,
    required this.name,
  });

  final int? id;
  final String? name;

  factory Brand.fromJson(Map<String, dynamic> json) => Brand(
        id: json["id"] ?? null,
        name: json["name"] ?? null,
      );
}

class Category {
  Category({
    required this.id,
    required this.name,
    required this.children,
  });

  final int id;
  final String name;
  final List<Child> children;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        children:
            List<Child>.from(json["children"].map((x) => Child.fromJson(x))),
      );
}

class Child {
  Child({
    required this.id,
    required this.iconUrl,
    required this.name,
    required this.parent,
  });

  final int id;
  final String iconUrl;
  final String name;
  final Brand parent;

  factory Child.fromJson(Map<String, dynamic> json) => Child(
        id: json["id"],
        iconUrl: json["icon_url"],
        name: json["name"],
        parent: Brand.fromJson(json["parent"]),
      );
}

class Links {
  Links({
    required this.count,
    required this.currentPage,
    required this.from,
    required this.to,
    required this.lastPage,
    required this.perPage,
    required this.total,
  });

  final int count;
  final int currentPage;
  final int from;
  final int to;
  final int lastPage;
  final int perPage;
  final int total;

  factory Links.fromJson(Map<String, dynamic> json) => Links(
        count: json["count"],
        currentPage: json["current_page"],
        from: json["from"],
        to: json["to"],
        lastPage: json["last_page"],
        perPage: json["per_page"],
        total: json["total"],
      );
}
