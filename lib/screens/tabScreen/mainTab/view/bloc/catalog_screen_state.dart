part of 'catalog_screen_bloc.dart';

abstract class CatalogScreenState extends Equatable {
  const CatalogScreenState();
  
  @override
  List<Object> get props => [];
}

class CatalogScreenInitial extends CatalogScreenState {}
