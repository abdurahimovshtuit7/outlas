import 'package:flutter/material.dart';
import 'package:outlus/components/card_ad.dart';
import 'package:outlus/constants/images.dart';

class HotDiscounts extends StatefulWidget {
  const HotDiscounts({Key? key}) : super(key: key);

  @override
  _HotDiscountsState createState() => _HotDiscountsState();
}

class _HotDiscountsState extends State<HotDiscounts>
    with AutomaticKeepAliveClientMixin<HotDiscounts> {
  late ScrollController _controller;
  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GridView.builder(
          controller: _controller,
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 3 / 6,
              crossAxisSpacing: 15,
              mainAxisSpacing: 15),
          itemCount: 10,
          itemBuilder: (BuildContext ctx, index) {
            if (index % 2 == 0) {
              return CardAd(
                'Tonny Black',
                'Серая замша Кроссовки унисекс',
                '246 300 UZS',
                '',
                '317 100 UZS',
                '30',
              );
            } else
              return CardAd(
                'Gucci',
                'Серая замша Кроссовки унисекс',
                '246 300 UZS',
                Images.gucci,
                '246 300 UZS',
              );
          }),
    );
  }
}
