import 'package:flutter/material.dart';
import 'package:outlus/screens/tabScreen/mainTab/view/brand_item.dart';

class PopularBrands extends StatefulWidget {
  const PopularBrands({Key? key}) : super(key: key);

  @override
  _PopularBrandsState createState() => _PopularBrandsState();
}

class _PopularBrandsState extends State<PopularBrands>
    with AutomaticKeepAliveClientMixin<PopularBrands> {
  late ScrollController _controller;
  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GridView.builder(
          controller: _controller,
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 5 / 3,
              crossAxisSpacing: 15,
              mainAxisSpacing: 15),
          itemCount: 15,
          itemBuilder: (BuildContext ctx, index) {
            return BrandItem();
          }),
    );
  }
}
