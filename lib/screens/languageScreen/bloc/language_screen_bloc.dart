import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'language_screen_event.dart';
part 'language_screen_state.dart';

class LanguageScreenBloc extends Bloc<LanguageScreenEvent, LanguageScreenState> {
  LanguageScreenBloc() : super(LanguageScreenInitial());

  @override
  Stream<LanguageScreenState> mapEventToState(
    LanguageScreenEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
