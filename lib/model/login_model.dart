// To parse this JSON data, do
//
//     final loginResponse = loginResponseFromJson(jsonString);

import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
    LoginResponse({
        required this.success,
        required this.data,
        this.links,
        this.errors,
    });

    final bool success;
    final Data data;
    final dynamic links;
    final dynamic errors;

    factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        success: json["success"],
        data: Data.fromJson(json["data"]),
        links: json["links"],
        errors: json["errors"],
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data.toJson(),
        "links": links,
        "errors": errors,
    };
}

class Data {
    Data({
       required this.accessToken,
       required this.tokenType,
       required this.expiresIn,
       required this.permissions,
       required this.roles,
       required this.user,
    });

    final String accessToken;
    final String tokenType;
    final int expiresIn;
    final List<dynamic> permissions;
    final List<String> roles;
    final User user;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        accessToken: json["access_token"],
        tokenType: json["token_type"],
        expiresIn: json["expires_in"],
        permissions: List<dynamic>.from(json["permissions"].map((x) => x)),
        roles: List<String>.from(json["roles"].map((x) => x)),
        user: User.fromJson(json["user"]),
    );

    Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "token_type": tokenType,
        "expires_in": expiresIn,
        "permissions": List<dynamic>.from(permissions.map((x) => x)),
        "roles": List<dynamic>.from(roles.map((x) => x)),
        "user": user.toJson(),
    };
}

class User {
    User({
       required this.id,
       required this.name,
       required this.phone,
       required this.email,
    });

    final int id;
    final String name;
    final String phone;
    final dynamic email;

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        phone: json["phone"],
        email: json["email"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phone": phone,
        "email": email,
    };
}
