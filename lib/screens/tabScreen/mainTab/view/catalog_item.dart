import 'package:flutter/material.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';

class CatalogItem extends StatelessWidget {
  const CatalogItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        height: 200,
        width: double.infinity,
        child: Stack(
          children: [
            Image.asset(
              Images.product,
              height: 200,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Container(
              margin: const EdgeInsets.only(top: 160),
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
              decoration: const BoxDecoration(
                color: AppColor.white,
              ),
              child: Text(
                'Кроссовки',
                style: TextStyle(
                  fontFamily: AppFont.SFThin,
                  fontSize: 18,
                  letterSpacing: 3,
                  fontWeight: FontWeight.w300
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
