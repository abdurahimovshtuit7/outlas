import 'package:outlus/screens/auth/codeEntry/bloc/code_entry_state.dart';

abstract class CodeEntryEvent {}

class CodeChanged extends CodeEntryEvent {
  final String code;

  CodeChanged({required this.code});
}

class WriteData extends CodeEntryEvent {
  final String username;
  final String name;
  final String lastname;
  final String gender;
  final String password;
  final String passwordConfirmation;

  WriteData({
    required this.username,
    required this.name,
    required this.lastname,
    required this.gender,
    required this.password,
    required this.passwordConfirmation,
  });
}

class ApprovePhone extends CodeEntryEvent {}
