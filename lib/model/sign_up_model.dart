// To parse this JSON data, do
//
//     final signUp = signUpFromJson(jsonString);

import 'dart:convert';

SignUpModel signUpFromJson(String str) => SignUpModel.fromJson(json.decode(str));

String signUpToJson(SignUpModel data) => json.encode(data.toJson());

class SignUpModel {
    SignUpModel({
        required this.success,
        required this.data,
        this.links,
        this.errors,
    });

    final bool success;
    final Data data;
    final dynamic links;
    final dynamic errors;

    factory SignUpModel.fromJson(Map<String, dynamic> json) => SignUpModel(
        success: json["success"],
        data: Data.fromJson(json["data"]),
        links: json["links"],
        errors: json["errors"],
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": data.toJson(),
        "links": links,
        "errors": errors,
    };
}

class Data {
    Data({
        required this.message,
    });

    final String message;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        message: json["message"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
    };
}
