import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:outlus/model/main_catalog_model.dart';
import 'package:outlus/utils/exceptions.dart';
import 'package:outlus/utils/header_options.dart';

class MainTabRepository {
  dynamic _data = '';

  // Future<void> fetchData() async {
  //   // simulate real data fetching
  //   await Future.delayed(Duration(milliseconds: 1600));
  //   // store dummy data
  //   _data = 'First Page';
  // }
 Future<dynamic> getMainCatalog() async {
    try {
      Response response = await HeaderOptions.dio.get(
        '/home/catalog',
        // data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      if (response.statusCode == 200) {
        var resData = MainCatalogModel.fromJson(response.data);
        _data = resData;
        // var data;
        List<Datum> dataList =  resData.data;
        // await SharedPref().save('token', resData.data.accessToken);
        // await SharedPref().save('userData', resData.data.user);
        // Map<String,dynamic> user = await SharedPref().read('userData');
        print(dataList[0].category.children[0].name);
      } else {
        // var _result = 'error code : ${response.statusCode}';
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();

      print('errorMessage: ${errorMessage}');
      throw Exception(errorMessage);
    }
  }
  dynamic get data => _data;
}
