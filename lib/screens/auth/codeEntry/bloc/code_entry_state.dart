// import 'package:social_media_app/auth/form_submission_status.dart';


import 'form_submission_status.dart';

class CodeEntryState {
  final String code;
  bool get isValidCode => code.length > 3;

  final String username;
  final String name;
  final String lastname;
  final String gender;
  final String password;
  final String passwordConfirmation;

  final FormSubmissionStatus formStatus;

  CodeEntryState({
    this.code = '',
    this.username = '',
    this.name = '',
    this.lastname = '',
    this.gender = '',
    this.password = '',
    this.passwordConfirmation = '',
    this.formStatus = const InitialFormStatus(),
  });

  CodeEntryState copyWith({
    String? code,
    String? username,
    String? name,
    String? lastname,
    String? gender,
    String? password,
    String? passwordConfirmation,
    FormSubmissionStatus? formStatus,
  }) {
    return CodeEntryState(
      code: code ?? this.code,
      username: username ?? this.username,
      name: name ?? this.name,
      lastname: lastname ?? this.lastname,
      gender: gender ?? this.gender,
      password: password ?? this.password,
      passwordConfirmation: passwordConfirmation ?? this.passwordConfirmation,
      formStatus: formStatus ?? this.formStatus,
    );
  }
}
