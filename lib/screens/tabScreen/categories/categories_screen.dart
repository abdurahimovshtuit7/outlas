import 'package:flutter/material.dart';
// import 'package:outlus/constants/colors.dart';
import 'package:outlus/screens/tabScreen/categories/view/all_item.dart';
import 'package:outlus/screens/tabScreen/categories/view/child_item.dart';
import 'package:outlus/screens/tabScreen/categories/view/parent_item.dart';
// import 'package:outlus/screens/tabScreen/mainTab/view/brand_item.dart';

class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen({Key? key}) : super(key: key);

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.24,
          padding: EdgeInsets.symmetric(vertical: 10),
          child: ListView.builder(
              itemCount: 15,
              itemBuilder: (context, index) {
                return ParentItem(index);
              }),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.76,
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                mainAxisSpacing: 12,
                crossAxisCount: 3,
                childAspectRatio: 3 / 6,
                crossAxisSpacing: 10,
              ),
              itemCount: 20,
              itemBuilder: (BuildContext ctx, index) {
                if (index == 19) {
                  return AllItem();
                } else
                  return ChildItem();
              }),
        ),
      ],
    );
  }
}
