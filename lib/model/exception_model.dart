// To parse this JSON data, do
//
//     final exceptionModel = exceptionModelFromJson(jsonString);

import 'dart:convert';

ExceptionModel exceptionModelFromJson(String str) => ExceptionModel.fromJson(json.decode(str));

String exceptionModelToJson(ExceptionModel data) => json.encode(data.toJson());

class ExceptionModel {
    ExceptionModel({
        required this.success,
        required this.data,
        required this.message,
        required this.errors,
    });

    final bool success;
    final List<dynamic> data;
    final dynamic message;
    final Errors errors;

    factory ExceptionModel.fromJson(Map<String, dynamic> json) => ExceptionModel(
        success: json["success"],
        data: List<dynamic>.from(json["data"].map((x) => x)),
        message: json["message"],
        errors: Errors.fromJson(json["errors"]),
    );

    Map<String, dynamic> toJson() => {
        "success": success,
        "data": List<dynamic>.from(data.map((x) => x)),
        "message": message,
        "errors": errors.toJson(),
    };
}

class Errors {
    Errors({
        required this.username,
    });

    final List<String> username;

    factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        username: List<String>.from(json["username"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "username": List<dynamic>.from(username.map((x) => x)),
    };
}
