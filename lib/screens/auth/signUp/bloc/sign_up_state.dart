// import 'package:social_media_app/auth/form_submission_status.dart';

import 'package:outlus/screens/auth/signUp/bloc/form_submission_status.dart';

class SignUpState {
  final String username;
  bool get isValidUsername => username.length > 3;

  final String lastname;
  bool get isValidLastname => lastname.isNotEmpty;

  final String phonenumber;
  bool get isValidPhonenumber => phonenumber.length >5;

  final String password;
  bool get isValidPassword => password.length > 5;

  final String confirmpassword;
  bool get isValidConfirmpassword => confirmpassword == password;

  final bool gender;
  // bool get isValidGender => gender


  final FormSubmissionStatus formStatus;

  SignUpState({
    this.username = '',
    this.lastname = '',
    this.phonenumber = '',
    this.password = '',
    this.confirmpassword = '',
    this.gender = true,
    this.formStatus = const InitialFormStatus(),
  });

  SignUpState copyWith({
    String? username,
    String? lastname,
    String? phonenumber,
    String? password,
    String? confirmpassword,
    bool? gender,

    FormSubmissionStatus? formStatus,
  }) {
    return SignUpState(
      username: username ?? this.username,
      lastname: lastname ?? this.lastname,
      phonenumber: phonenumber ?? this.phonenumber,
      password: password ?? this.password,
      confirmpassword: confirmpassword ?? this.confirmpassword,
      gender: gender??this.gender,
      formStatus: formStatus ?? this.formStatus,
    );
  }
}
