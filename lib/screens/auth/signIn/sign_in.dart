import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:outlus/components/app_bar.dart';
import 'package:outlus/components/button.dart';
import 'package:outlus/components/icon_button.dart';
import 'package:outlus/components/input.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:outlus/constants/images.dart';
import 'package:outlus/repositories/auth_repository.dart';
import 'package:outlus/screens/auth/signIn/bloc/form_submission_status.dart';
import 'package:outlus/screens/auth/signIn/bloc/login_bloc.dart';
import 'package:outlus/screens/auth/signIn/bloc/login_event.dart';
import 'package:outlus/screens/auth/signIn/bloc/login_state.dart';

import '../../../routes.dart';

class LoginPage extends StatefulWidget {
  static const routeName = 'sign-in';

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void onPress() {
    if (_formKey.currentState!.validate()) {
      context.read<LoginBloc>().add(LoginSubmitted());
    }
  }

  void _showSnackBar(BuildContext context, dynamic message) {
    // print(message["username"](0));
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Header('Войти в систему'.tr(), true),
      // resizeToAvoidBottomInset: false,
      body: SafeArea(
          bottom: false,
          child: BlocProvider(
            create: (context) => LoginBloc(
              authRepo: context.read<AuthRepository>(),
            ),
            child: BlocListener<LoginBloc, LoginState>(
              listener: (BuildContext context, LoginState state) {
                final formStatus = state.formStatus;
                if (formStatus is SubmissionFailed) {
                  _showSnackBar(context, formStatus.exception.toString());
                } else if (formStatus is SubmissionSuccess) {
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil(Routes.tabs, (route) => false);
                }
              },
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  // reverse: true,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: BlocBuilder<LoginBloc, LoginState>(
                      builder: (BuildContext context, LoginState state) {
                        return Column(
                          // crossAxisAlignment: CrossAxisAlignment.end,
                          // mainAxisAlignment: MainAxisAlignment.spaceBetwe,
                          children: [
                            Container(                              
                              margin: const EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 16),
                              child: Text(
                                'Чтобы войти в систему введите свой номер телефона или адрес электронной почты и пароль',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: AppFont.SFRegular,
                                ),
                              ),
                            ),
                            //Component for TextInput
                            InputField(
                              'Номер телефона или адрес Эл. почты'.tr(),
                              false,
                              (value) => context.read<LoginBloc>().add(
                                    LoginUsernameChanged(username: value),
                                  ),
                              (value) => state.isValidUsername
                                  ? null
                                  : 'Username is too short',
                            ),
                            //Component for TextInput
                            InputField(
                              'Пароль'.tr(),
                              true,
                              (value) => context.read<LoginBloc>().add(
                                    LoginPasswordChanged(password: value),
                                  ),
                              (value) => state.isValidPassword
                                  ? null
                                  : 'Password is too short',
                            ),
                            Align(
                              alignment: Alignment.centerRight,
                              child: TextButton(
                                onPressed: () {
                                  Navigator.of(context)
                                      .pushNamed(Routes.forgotPassword);
                                },
                                child: Text(
                                  'Забыли пароль?',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      color: AppColor.pink,
                                      fontFamily: AppFont.SFRegular),
                                ).tr(),
                              ),
                            ),
                            //Component for Button
                            Button("Войти".tr(), () {
                              if (_formKey.currentState!.validate()) {
                                context.read<LoginBloc>().add(LoginSubmitted());
                              }
                            }, AppColor.pink, AppColor.white, 0,
                                state.formStatus is FormSubmitting),
                            Center(
                                child: Text('У вас нет аккаунта?',
                                        style: TextStyle(
                                            fontFamily: AppFont.SFRegular))
                                    .tr()),
                            Button(
                              "Создать аккаунт".tr(),
                              () {
                                Navigator.of(context).pushNamed(Routes.signUp);
                              },
                              AppColor.white,
                              AppColor.pink,
                              1,
                            ),
                            ButtonWithIcon(
                              Images.facebookLogo,
                              'Log in with Facebook'.tr(),
                              onPress,
                              AppColor.warmGrey,
                              AppColor.black,
                            ),
                            ButtonWithIcon(
                              Images.googleLogo,
                              'Log in with Google'.tr(),
                              onPress,
                              AppColor.warmGrey,
                              AppColor.black,
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
