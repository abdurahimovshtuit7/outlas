import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';
import 'package:outlus/routes.dart';
import 'package:outlus/screens/onBoarding/view/onboarding.dart';
import 'package:outlus/screens/tabScreen/bloc/bottom_navigation_bloc.dart';
import 'package:outlus/screens/tabScreen/categories/categories_screen.dart';
import 'package:outlus/screens/tabScreen/mainTab/main_tab.dart';
import 'package:outlus/screens/tabScreen/profile/profile.dart';
import 'package:outlus/screens/tabScreen/view/app_bar.dart';
import 'package:easy_localization/easy_localization.dart';

class TabsScreen extends StatefulWidget {
  static const String routeName = 'tabs';
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen>
    with
        SingleTickerProviderStateMixin,
        AutomaticKeepAliveClientMixin<TabsScreen> {
  final List<Map<String, dynamic>> _pages = [
    {
      'page': MainTab(),
      'title': 'Домой',
    },
    {
      'page': MainTab(),
      'title': 'Избранные',
    },
    {
      'page': CategoriesScreen(),
      'title': 'Категории',
    },
    {
      'page': MainTab(),
      'title': 'Акции',
    },
    {
      'page': ProfileScreen(),
      'title': 'Профиль',
    },
  ];

  late BottomNavigationBloc bloc;
  late TabController _controller;

  @override
  void initState() {
    bloc = BlocProvider.of<BottomNavigationBloc>(context);
    _controller = TabController(length: 5, vsync: this);

    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    _controller.dispose();

    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  _tabBarBody(body) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) => MaterialPageRoute(
        builder: (context) => body,
        settings: settings,
      ),
      
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var selectedPage =
        context.select((BottomNavigationBloc bloc) => bloc.currentIndex);
    return Scaffold(
      appBar: selectedPage != 4
          ? CustomAppBar(_pages[selectedPage]['title'.tr()], selectedPage)
          : null,
      // body: _pages[selectedPage]['page'],
      body: IndexedStack(
        index: selectedPage,
        children: [
          _tabBarBody(MainTab()),
          _tabBarBody(MainTab()),
          _tabBarBody(MainTab()) ,
          _tabBarBody(CategoriesScreen()) ,
          _tabBarBody(ProfileScreen())

//  Navigator.pushNamed(context, Routes.profileScreen)

          // Navigator.of(context).pushNamed(Routes.profileScreen)
        ],
      ),
      // body: BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
      //   // buildWhen: (previous, current) {
      //   //   print("Call buildWhen(previous: $previous, current: $current)");
      //   //   return current is MainTabLoaded ||
      //   //       current is FavoriteTabLoaded ||
      //   //       current is CategoriesTabLoaded ||
      //   //       current is PromotionTabLoaded ||
      //   //       current is MoreTabLoaded;
      //   // },
      //   buildWhen: (previous, current) {
      //     return true;
      //   },
      //   builder: (BuildContext context, BottomNavigationState state) {
      //     print("State:$state");
      //     if (state is PageLoading) {
      //       return Center(child: CircularProgressIndicator());
      //     }
      //     if (state is MainTabLoaded) {
      //       return MainTab();
      //     }
      //     if (state is FavoriteTabLoaded) {
      //       return const CategoriesScreen();
      //     }
      //     if (state is CategoriesTabLoaded) {
      //       return const CategoriesScreen();
      //     }
      //     if (state is PromotionTabLoaded) {
      //       return const CategoriesScreen();
      //     }
      //     if (state is MoreTabLoaded) {
      //       return const ProfileScreen();
      //     }
      //     return Container();
      //   },
      // ),
      bottomNavigationBar:
          BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
        builder: (BuildContext context, BottomNavigationState state) {
          var selectedIndex =
              context.select((BottomNavigationBloc bloc) => bloc.currentIndex);
          return BottomNavigationBar(
            onTap: (index) {
              context
                  .read<BottomNavigationBloc>()
                  .add(PageTapped(index: index));
            },
            backgroundColor: AppColor.white,
            unselectedItemColor: AppColor.coolGrey,
            selectedItemColor: AppColor.pink,
            currentIndex: selectedIndex,
            selectedFontSize: 12,
            unselectedFontSize: 12,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  Images.homeIcon,
                  color: selectedIndex == 0 ? AppColor.pink : AppColor.coolGrey,
                ),
                label: 'Домой'.tr(),
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  Images.favouriteIcon,
                  color: selectedIndex == 1 ? AppColor.pink : AppColor.coolGrey,
                ),
                label: 'Избранные'.tr(),
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  Images.menu,
                  color: selectedIndex == 2 ? AppColor.pink : AppColor.coolGrey,
                ),
                label: 'Категории'.tr(),
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  Images.rebateIcon,
                  color: selectedIndex == 3 ? AppColor.pink : AppColor.coolGrey,
                ),
                label: 'Скидки'.tr(),
              ),
              BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  Images.accountIcon,
                  color: selectedIndex == 4 ? AppColor.pink : AppColor.coolGrey,
                ),
                label: 'Профиль'.tr(),
              ),
            ],
          );
        },
      ),
    );
  }
}
