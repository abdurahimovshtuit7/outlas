import 'package:flutter/material.dart';
import 'package:outlus/constants/colors.dart';

class Badge extends StatelessWidget {
  final Widget child;
  final int count;
   Badge(this.child,this.count);

  @override
  Widget build(BuildContext context) {
    return  Stack(
                alignment: Alignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: child,
                  ),
                  Positioned(
                    left: 15,
                    top:18,
                    child: Container(
                      padding: const EdgeInsets.all(2.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: AppColor.black,
                      ),
                      constraints: const BoxConstraints(
                        minWidth: 16,
                        minHeight: 16,
                      ),
                      child: Text(
                        count.toString(),
                        textAlign: TextAlign.center,
                        style:  TextStyle(
                          fontSize: 10,
                          color:AppColor.white,
                        ),
                      ),
                    ),
                  )
                ],
              );
  }
}