import 'package:dio/dio.dart';
import 'package:outlus/utils/shared_pref.dart';

class HeaderOptions {
  static var uri = "https://api.outlas.uz/api/v1/mobile";
  


  static BaseOptions options = BaseOptions(
    baseUrl: uri,
    responseType: ResponseType.json,
    connectTimeout: 5000,
    receiveTimeout: 3000,
  );

  static var dio = Dio(options);

   Future<Options> option() async {
    String uuid = await SharedPref().read('uuid');
    String deviceName = await SharedPref().read('deviceName');
    String gender = await SharedPref().read('gender');

    return Options(
      headers: {
        "Accept": 'application/json',
        "Content-Type": "application/json",
        "uuid": uuid,
        "deviceName": deviceName,
        "Gender": gender,
        "appVersion": "3.002",
        "typeOS": "Android",
      },
      contentType: Headers.formUrlEncodedContentType,
    );
  }
  
}
