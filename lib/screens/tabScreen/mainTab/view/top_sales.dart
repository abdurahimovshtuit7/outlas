import 'package:flutter/material.dart';
import 'package:outlus/components/card_ad.dart';
import 'package:outlus/constants/images.dart';

class TopSales extends StatefulWidget {
  const TopSales({Key? key}) : super(key: key);

  @override
  _TopSalesState createState() => _TopSalesState();
}

class _TopSalesState extends State<TopSales>
    with AutomaticKeepAliveClientMixin<TopSales> {
  late ScrollController _controller;
  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GridView.builder(
          controller: _controller,
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 3 / 6,
              crossAxisSpacing: 15,
              mainAxisSpacing: 15),
          itemCount: 10,
          itemBuilder: (BuildContext ctx, index) {
            return CardAd(
              'Gucci',
              'Серая замша Кроссовки унисекс',
              '246 300 UZS',
              Images.gucci,
              '246 300 UZS',
            );
          }),
    );
  }
}
