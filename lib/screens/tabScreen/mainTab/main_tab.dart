import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';
import 'package:outlus/repositories/categories_tab_repository.dart';
import 'package:outlus/repositories/repositories.dart';
import 'package:outlus/screens/tabScreen/bloc/bottom_navigation_bloc.dart';
import 'package:outlus/screens/tabScreen/mainTab/view/catalog_screen.dart';
import 'package:outlus/screens/tabScreen/mainTab/view/hot_discounts.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:outlus/screens/tabScreen/mainTab/view/popular_brands.dart';
import 'package:outlus/screens/tabScreen/mainTab/view/top_sales.dart';

// import 'package:outlus/screens/tabScreen/view/scrollable_tab.dart';

class MainTab extends StatefulWidget {
  // final dynamic data;
  const MainTab({
    Key? key,
  }) : super(key: key);

  @override
  _MainTabState createState() => _MainTabState();
}

class _MainTabState extends State<MainTab>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin<MainTab> {
  final List a = [
    'КАТАЛОГ',
    'ГОРЯЧИЕ СКИДКИ',
    'ТОП ПРОДАЖ',
    'ПОПУЛЯРНЫЕ БРЕНДЫ',
    'ПРЕМИУМ БРЕНДЫ'
  ];

  final PageStorageBucket _bucket = PageStorageBucket();

  late TabController _tabController;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: a.length, vsync: this, initialIndex: _getInitialIndex());
    _tabController.addListener(_handleTabSelection);
  }

  int _getInitialIndex() {
    int initialIndex = PageStorage.of(context)
            ?.readState(context, identifier: const ValueKey('index')) ??
        0;
    return initialIndex;
  }

  void _handleTabSelection() {
    setState(() {
      PageStorage.of(context)?.writeState(context, _tabController.index,
          identifier: const ValueKey('index'));
    });
  }

  Widget showTab(String value, int index) {
    if (index == 4) {
      return Tab(
        child: Container(
          child: Align(
              alignment: Alignment.center,
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: SvgPicture.asset(
                      Images.crown,
                      height: 24,
                      width: 24,
                    ),
                  ),
                  const SizedBox(width: 6),
                  Text(
                    value,
                  ).tr(),
                ],
              )),
        ),
      );
    }
    return Tab(
      child: Container(
        child: Align(
          alignment: Alignment.center,
          child: Text(
            value,
          ).tr(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    print("object:${_tabController.index}");

    return PageStorage(
      bucket: _bucket,
      child: DefaultTabController(
        length: a.length,
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight),
            child: TabBar(
              controller: _tabController,
              indicatorColor: AppColor.white,
              unselectedLabelColor: Colors.grey,
              labelColor: AppColor.black,
              isScrollable: true,
              tabs: [
                ...a.asMap().entries.map((entry) {
                  int index = entry.key;
                  String value = entry.value;
                  return showTab(value, index);
                }),
              ],
            ),
          ),
          body: TabBarView(
            // controller: _tabController,
            children: [
              BlocProvider<BottomNavigationBloc>(
                lazy: false,
                create: (context) => BottomNavigationBloc(
                  mainTabRepository: MainTabRepository(),
                  favoriteTabRepository: FavoriteTabRepository(),
                  categoriesTabRepository: CategoriesTabRepository(),
                  promotionTabRepository: PromotionTabRepository(),
                  moreTabRepository: MoreTabRepository(),
                )..add(AppStarted()),
                child: CatalogScreen(),
              ),
              const HotDiscounts(),
              const TopSales(),
              const PopularBrands(),
              const PopularBrands(),
            ],
          ),
        ),
      ),
    );
  }
}
