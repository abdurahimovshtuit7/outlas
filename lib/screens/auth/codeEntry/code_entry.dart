import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outlus/components/button.dart';
import 'package:outlus/components/input.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:outlus/repositories/auth_repository.dart';

import '../../../routes.dart';
import 'bloc/code_entry_bloc.dart';
import 'bloc/code_entry_event.dart';
import 'bloc/code_entry_state.dart';
import 'bloc/form_submission_status.dart';

class CodeEntry extends StatefulWidget {
  static const routeName = 'code-entry';

  final String name;
  final String lastname;
  final String username;
  final String gender;
  final String password;
  final String passwordConfirmation;

  CodeEntry({
    required this.name,
    required this.lastname,
    required this.username,
    required this.gender,
    required this.password,
    required this.passwordConfirmation,
  });

  @override
  State<CodeEntry> createState() => _CodeEntryState();
}

class _CodeEntryState extends State<CodeEntry> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final interval = const Duration(seconds: 1);
  final int timerMaxSeconds = 10;
  int currentSeconds = 0;

  String get timerText =>
      '${((timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')} : ${((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  void onPress() {
    // if (_formKey.currentState!.validate()) {
    //   context.read<LoginBloc>().add(LoginSubmitted());
    // }
  }
  startTimeout([int? milliseconds]) {
    var duration = interval;
    Timer.periodic(duration, (timer) {
      setState(() {
        // print(timer.tick);
        currentSeconds = timer.tick;
        if (timer.tick >= timerMaxSeconds) timer.cancel();
      });
    });
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    startTimeout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Map<String, dynamic> data = {
    //   "username": widget.username,
    //   "name": widget.name,
    //   "last_name": widget.lastname,
    //   "gender": widget.gender,
    //   "password": widget.password,
    //   "password_confirmation": widget.passwordConfirmation,
    // };
    // print(widget.username);
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        title: const Text(
          'Код подтверждения',
          style: TextStyle(color: AppColor.segmentTextColor),
        ).tr(),
        elevation: 1,
        leading: IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: const Icon(
              Icons.close_rounded,
              color: AppColor.segmentTextColor,
            )),
        // automaticallyImplyLeading: false, // Used for removing back buttoon.
      ),
      body: SafeArea(
          bottom: false,
          child: BlocProvider(
            create: (context) => CodeEntryBloc(
              authRepo: context.read<AuthRepository>(),
            ),
            child: BlocListener<CodeEntryBloc, CodeEntryState>(
              listener: (BuildContext context, CodeEntryState state) {
                final formStatus = state.formStatus;
                if (formStatus is SubmissionFailed) {
                  _showSnackBar(context, formStatus.exception.toString());
                } else if (formStatus is SubmissionSuccess) {
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil(Routes.tabs, (route) => false);
                }
              },
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  // reverse: true,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: BlocBuilder<CodeEntryBloc, CodeEntryState>(
                      builder: (BuildContext context, CodeEntryState state) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(top: 20),
                              child: Text(
                                'Введите код',
                                style: TextStyle(
                                    fontSize: 12,
                                    fontFamily: AppFont.SFRegular),
                              ).tr(),
                            ),
                            //Component for TextInput
                            InputField(
                                ''.tr(),
                                false,
                                (value) => context.read<CodeEntryBloc>().add(
                                      CodeChanged(code: value),
                                    ),
                                (value) => state.isValidCode
                                    ? null
                                    : 'Code is too short',
                                keyboardType: TextInputType.number),

                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  TextButton(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Не получили еще письмо с кодом",
                                            style: TextStyle(
                                                fontFamily: AppFont.SFRegular,
                                                fontSize: 14,
                                                color: currentSeconds ==
                                                        timerMaxSeconds
                                                    ? AppColor.black
                                                    : AppColor.black
                                                        .withOpacity(0.3)),
                                          ).tr(),
                                          SizedBox(height: 10),
                                          Text(
                                            'отправить заново',
                                            style: TextStyle(
                                              fontFamily: AppFont.SFRegular,
                                              fontSize: 14,
                                              color: currentSeconds ==
                                                      timerMaxSeconds
                                                  ? AppColor.pink
                                                  : AppColor.pink
                                                      .withOpacity(0.4),
                                              decoration:
                                                  TextDecoration.underline,
                                            ),
                                          ).tr(),
                                        ],
                                      ),
                                      onPressed:
                                          currentSeconds == timerMaxSeconds
                                              ? startTimeout
                                              : null),
                                  Text(timerText)
                                ]),

                            //Component for Button
                            Button("Подтвердить".tr(), () {
                              if (_formKey.currentState!.validate()) {
                                context.read<CodeEntryBloc>().add(
                                      WriteData(
                                        username: widget.username,
                                        name: widget.name,
                                        lastname: widget.lastname,
                                        gender: widget.gender,
                                        password: widget.password,
                                        passwordConfirmation:
                                           widget.passwordConfirmation,
                                      ),
                                    );
                                context
                                    .read<CodeEntryBloc>()
                                    .add(ApprovePhone());
                              }
                            }, AppColor.pink, AppColor.white, 0,
                                state.formStatus is FormSubmitting),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
