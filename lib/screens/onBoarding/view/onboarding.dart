import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outlus/constants/app_font.dart';

import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';
import 'package:outlus/routes.dart';
import 'package:outlus/screens/onBoarding/bloc/onboarding_bloc.dart';
import 'package:outlus/screens/onBoarding/view/page.dart';
import 'package:outlus/screens/onBoarding/view/row_button.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:outlus/utils/shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class OnboardingScreen extends StatefulWidget {
  static const String routeName = 'onboarding';

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  late OnboardingBloc bloc;
  late var v1;
  var uuid = const Uuid();
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  @override
  void initState() {
    bloc = BlocProvider.of<OnboardingBloc>(context);
    v1 = uuid.v1();
    initPlatformState();
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  void buttonPressed(int number) {
    setState(() {
      _currentPage = number;
      _pageController.animateToPage(
        number,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    });
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData = <String, dynamic>{};

    try {
      if (Platform.isAndroid) {
        // AndroidDeviceInfo deviceData1 = await deviceInfoPlugin.androidInfo;

        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        // IosDeviceInfo deviceData1 = await deviceInfoPlugin.iosInfo;

        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    setState(() {
      _deviceData = deviceData;
    });
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  // _setStorage() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   int counter = (prefs.getInt('counter') ?? 0) + 1;
  //   print('Pressed $counter times.');
  //   await prefs.setInt('counter', counter);
  // }

  var list = [
    {"source": Images.boyImage, "key": "m", "title": "Для мужчин".tr()},
    {"source": Images.girlImage, "key": "f", "title": "Для женщин".tr()},
    {"source": Images.childImage, "key": "ch", "title": "Для детей".tr()},
  ];

  void _saveStorage() async {
    String uuid = v1;
    String deviceName = _deviceData['model'] + ' ' + _deviceData['version.sdkInt'].toString();
    String gender = list[_currentPage]['key'].toString();

    await SharedPref().save('uuid', uuid);
    await SharedPref().save('deviceName',deviceName);
    await SharedPref().save('gender', gender);
    Navigator.of(context).pushReplacementNamed(Routes.tabs);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: BlocBuilder<OnboardingBloc, OnboardingState>(
        builder: (context, state) {
          return Container(
            height: double.infinity,
            child: Stack(
              alignment: Alignment.center,
              children: [
                PageView(
                  physics: const ClampingScrollPhysics(),
                  controller: _pageController,
                  onPageChanged: (int page) {
                    setState(() {
                      _currentPage = page;
                    });
                  },
                  children: [
                    ...list.asMap().entries.map((entry) {
                      dynamic value = entry.value;
                      return OnboardingPage(value['source']);
                    }),
                  ],
                ),
                Positioned(
                  bottom: MediaQuery.of(context).size.height * 0.2,
                  child: TextButton(
                    child: Text(
                      'Посмотреть',
                      style: TextStyle(fontFamily: AppFont.SFRegular),
                    ).tr(),
                    style: TextButton.styleFrom(
                      primary: AppColor.white,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 12),
                      side: BorderSide(color: AppColor.white, width: 1),
                    ),
                    onPressed: () {
                      _saveStorage();
                      // Navigator.of(context).pushReplacementNamed(Routes.tabs);
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ...list.asMap().entries.map((entry) {
                          int index = entry.key;
                          dynamic value = entry.value;
                          return RowButton(
                            _currentPage,
                            index,
                            value['title'],
                            () => buttonPressed(index),
                          );
                        }),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
