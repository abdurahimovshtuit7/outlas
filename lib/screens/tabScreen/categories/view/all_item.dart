import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';

class AllItem extends StatelessWidget {
  const AllItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        decoration: BoxDecoration(
          color: AppColor.white,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            width: 1,
            color: AppColor.warmGrey.withOpacity(0.4),
            style: BorderStyle.solid,
          ),
        ),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 104,
              alignment: Alignment.topRight,
              decoration: BoxDecoration(
                color: AppColor.white,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(4.0),
                  topRight: Radius.circular(4.0),
                ),
              ),
              child: Center(child: SvgPicture.asset(Images.allIcon)),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 6),
              child: Container(
                child: Text(
                  'ВСЕ',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: AppFont.SFRegular,
                    fontSize: 12,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
