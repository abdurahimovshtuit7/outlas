import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:outlus/repositories/repositories.dart';
// import 'package:outlus/screens/tabScreen/bloc/bottom_navigation_bloc.dart';
import 'package:outlus/screens/tabScreen/mainTab/view/catalog_item.dart';

class CatalogScreen extends StatefulWidget {
  CatalogScreen({Key? key}) : super(key: key);

  @override
  _CatalogScreenState createState() => _CatalogScreenState();
}

class _CatalogScreenState extends State<CatalogScreen>
    with AutomaticKeepAliveClientMixin<CatalogScreen> {
  late ScrollController _controller;

  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    _controller = ScrollController();
    super.initState();
  }
@override
void didChangeDependencies() {
  print("catalog");
  super.didChangeDependencies();
}  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView.builder(
        controller: _controller,
        itemCount: 10,
        itemBuilder: (context, index) {
          return CatalogItem();
        });
  }
}
