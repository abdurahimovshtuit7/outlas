import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:outlus/repositories/auth_repository.dart';
import 'package:outlus/screens/auth/forgotPassword/forgot_password.dart';
import 'package:outlus/screens/auth/signIn/sign_in.dart';
import 'package:outlus/screens/auth/signUp/sign_up.dart';
import 'package:outlus/screens/languageScreen/language_screen.dart';
import 'package:outlus/screens/onBoarding/bloc/onboarding_bloc.dart';
import 'package:outlus/screens/onBoarding/view/onboarding.dart';
import 'package:outlus/screens/tabScreen/profile/profile.dart';
import 'package:outlus/screens/tabScreen/tab_screen.dart';

import 'repositories/categories_tab_repository.dart';
import 'repositories/favorite_tab_repository.dart';
import 'repositories/main_tab_repository.dart';
import 'repositories/more_tab_repository.dart';
import 'repositories/promotion_tab_repository.dart';
import 'screens/tabScreen/bloc/bottom_navigation_bloc.dart';

class Routes {
  Routes._();

  //static variables
  static const String onBoarding = OnboardingScreen.routeName;
  static const String tabs = TabsScreen.routeName;
  static const String signIn = LoginPage.routeName;
  static const String signUp = SignUp.routeName;
  static const String forgotPassword = ForgotPassword.routeName;
  static const String languageScreen = LanguageScreen.routeName;
  static const String profileScreen = ProfileScreen.routeName;

  static final routes = <String, WidgetBuilder>{
    tabs: (BuildContext context) => BlocProvider<BottomNavigationBloc>(
          lazy: false,
          create: (context) => BottomNavigationBloc(
            mainTabRepository: MainTabRepository(),
            favoriteTabRepository: FavoriteTabRepository(),
            categoriesTabRepository: CategoriesTabRepository(),
            promotionTabRepository: PromotionTabRepository(),
            moreTabRepository: MoreTabRepository(),
          )..add(AppStarted()),
          child: TabsScreen(),
        ),
    onBoarding: (BuildContext context) => BlocProvider<OnboardingBloc>(
          create: (context) => OnboardingBloc(),
          child: OnboardingScreen(),
        ),
    signIn: (BuildContext context) => RepositoryProvider(
          create: (context) => AuthRepository(),
          child: LoginPage(),
        ),
    signUp:  (BuildContext context) => RepositoryProvider(
          create: (context) => AuthRepository(),
          child: SignUp(),
        ),
    forgotPassword: (BuildContext context) => ForgotPassword(),
    languageScreen: (BuildContext context) => const LanguageScreen(),
    profileScreen: (BuildContext context) => ProfileScreen()
  };
}
