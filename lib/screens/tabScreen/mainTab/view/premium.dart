import 'package:flutter/material.dart';
import 'package:outlus/screens/tabScreen/mainTab/view/brand_item.dart';
class PopularBrands extends StatefulWidget {
  const PopularBrands({ Key? key }) : super(key: key);

  @override
  _PopularBrandsState createState() => _PopularBrandsState();
}

class _PopularBrandsState extends State<PopularBrands> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 4 / 2,
              crossAxisSpacing: 15,
              mainAxisSpacing: 15),
          itemCount: 15,
          itemBuilder: (BuildContext ctx, index) {
           return BrandItem();
          }),
    );
  }
}