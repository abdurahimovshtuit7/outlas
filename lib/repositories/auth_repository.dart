import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
// import 'package:one_context/one_context.dart';
// import 'package:outlus/model/exception_model.dart';
import 'package:outlus/model/login_model.dart';
import 'package:outlus/model/sign_up_model.dart';
import 'package:outlus/utils/exceptions.dart';
// import 'package:outlus/routes.dart';
import 'package:outlus/utils/header_options.dart';
import 'package:outlus/utils/shared_pref.dart';

class AuthRepository {
  Future<dynamic> login(String username, String password) async {
    try {
      var data = {
        "username": username,
        "password": password,
        "remember": 1,
      };

      Response response = await HeaderOptions.dio.post(
        '/auth/login',
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );

      if (response.statusCode == 200) {
        var resData = LoginResponse.fromJson(response.data);
        
        await SharedPref().save('token', resData.data.accessToken);
        await SharedPref().save('userData', resData.data.user);
        // Map<String,dynamic> user = await SharedPref().read('userData');
        // print(user['name']);
      } else {
        // var _result = 'error code : ${response.statusCode}';
        print('_result');
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();

      print(errorMessage);
      throw Exception(errorMessage);
    }
  }

  Future<dynamic> signUp(String username, String lastname, String phonenumber,
      String password, String confirmPassword, bool gender) async {
    String genderName = gender ? 'm' : 'f';
    try {
      var data = {
        "username": phonenumber,
        "name": username,
        "last_name": lastname,
        "gender": genderName,
        "password": password,
        "password_confirmation": confirmPassword,
      };

      Response response = await HeaderOptions.dio.post(
        '/auth/sign-up',
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );

      if (response.statusCode == 200) {
        var resData = SignUpModel.fromJson(response.data);
        // await SharedPref().save('token',resData.data.accessToken);
        // await SharedPref().save('userData',resData.data.user);
        // Map<String,dynamic> user = await SharedPref().read('userData');
        print(resData);
      } else {
        // var _result = 'error code : ${response.statusCode}';
        print('_result');
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();

      print(errorMessage);
      throw Exception(errorMessage);
    }
  }

  Future<dynamic> approvePhone(
    String code,
    String username,
    String name,
    String lastname,
    String gender,
    String password,
    String passwordConfirmation,
  ) async {
    try {
      var data = {
        "code": code,
        "username": username,
        "name": name,
        "last_name": lastname,
        "gender": gender,
        "password": password,
        "password_confirmation": passwordConfirmation,
      };
       print(data);

      Response response = await HeaderOptions.dio.post(
        '/auth/confirm-user',
        data: json.encode(data),
        options: await HeaderOptions().option(),
      );
      if (response.statusCode == 200) {
        // var resData = LoginResponse.fromJson(response.data);
        // await SharedPref().save('token', resData.data.accessToken);
        // await SharedPref().save('userData', resData.data.user);
        // Map<String,dynamic> user = await SharedPref().read('userData');
        // print(user['name']);
      } else {
        // var _result = 'error code : ${response.statusCode}';
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();

      print('errorMessage: ${errorMessage}');
      throw Exception(errorMessage);
    }
  }
}
