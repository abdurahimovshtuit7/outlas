import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:outlus/constants/app_font.dart';
import 'package:outlus/constants/colors.dart';
import 'package:outlus/constants/images.dart';

class Header extends StatefulWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;
  final String title;
  final bool backButton;

  Header(this.title, this.backButton) : preferredSize = Size.fromHeight(50.0);

  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  @override
  Widget _text(title) {
    return Text(
      title,
      style: TextStyle(
          fontFamily: AppFont.SFSemibold,
          fontSize: 17,
          color: AppColor.black,
          fontWeight: FontWeight.w600),
    );
  }

  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return Container(
      height: 80,
      decoration: const BoxDecoration(
          border: Border(
        bottom: BorderSide(
          color: AppColor.warmGrey,
          width: 0.5,
        ),
      )),
      child: Padding(
        padding: const EdgeInsets.only(top: 22.0),
        child: widget.backButton
            ? Row(
                children: [
                  IconButton(
                    icon: SvgPicture.asset(Images.leftArrow),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: mediaQuery.size.width * 0.23),
                    child: _text(widget.title),
                  )
                ],
              )
            : Center(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: _text(widget.title),
                ),
              ),
      ),
    );
  }
}
